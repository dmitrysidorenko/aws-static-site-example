const AWS = require("aws-sdk");
const { STATUS_CODES } = require("http");
const DEFAULT_CONFIG = require("./config_default.json");

const BUCKET_NAME = "super-static-on-edge20191002213527684800000003";
const FILE_KEY = "config.json";

const logger = {
  error: (...args) => {
    console.log("[ERROR]", ...args);
  },
  debug: (...args) => {
    console.log("[DEBUG]", ...args);
  },
  log: (...args) => {
    console.log("[LOG]", ...args);
  },
  info: (...args) => {
    console.log("[INFO]", ...args);
  }
};

const loadConfig = () => {
  return new Promise((resolve, reject) => {
    const s3 = new AWS.S3({ region: "us-east-1" });
    const params = {
      Bucket: BUCKET_NAME,
      Key: FILE_KEY
    };
    logger.info("s3.getObject", params);
    s3.getObject(params, (err, data) => {
      if (err) {
        logger.error("s3.getObject", err);
        reject(err);
        return;
      }

      const configJsonString = data.Body.toString("utf-8");

      logger.info("Config", configJsonString);

      const config = JSON.parse(configJsonString);
      config.routings = config.routings.map(({ source, destination }) => ({
        source: new RegExp(source),
        text: source,
        destination
      }));
      resolve(config);
    });
  });
};

exports.handler = (evt, ctx, cb) => {
  const { request } = evt.Records[0].cf;
  logger.info({ request });
  loadConfig()
    .catch(err => {
      logger.error("Failed to load config", err);
      logger.info("Use default config", DEFAULT_CONFIG);
      return DEFAULT_CONFIG;
    })
    .then(({ routings }) => {
      const route = routings.find(({ source }) => source.test(request.uri));

      logger.info(
        "Route found",
        route && { source: route.text, destination: route.destination }
      );

      if (route) {
        const uri = request.uri.replace(route.source, route.destination);
        if (route.redirect) {
          return cb(null, redirect(uri));
        }
        request.uri = uri;
      }

      cb(null, request);
    })
    .catch(err => {
      logger.error(err);
      cb(err);
    });
};

function redirect(to, permanent = true) {
  const status = permanent ? "301" : "302";
  return {
    status,
    statusDescription: STATUS_CODES[status],
    headers: {
      location: [{ key: "Location", value: to }]
    }
  };
}
