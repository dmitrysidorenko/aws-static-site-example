#!/bin/bash

if [ ! -z $1 ]; then
  export AWS_DEFAULT_PROFILE=$1
fi

MS_WEBSITE="ms_website"
TREASURE_WEBSITE="treasure_website"

cd ./infra
MS_BUCKET_NAME=$(terraform output ms_bucket_name)
TREASURE_BUCKET_NAME=$(terraform output treasure_bucket_name)
CONFIG_BUCKET_NAME=$(terraform output config_bucket_name)
CLOUDFRONT_ID=$(terraform output cloudfront_id)
CLOUDFRONT_DOMAIN=$(terraform output cloudfront_domain)
cd ../

cd ./${MS_WEBSITE}
echo "Build web app First"
yarn build
echo "Upload build"
aws s3 sync ./build s3://$MS_BUCKET_NAME \
  --delete
cd ../

cd ./${TREASURE_WEBSITE}
echo "Build web app Second"
PUBLIC_URL=/treasure yarn build
echo "Upload build"
aws s3 sync ./build s3://$TREASURE_BUCKET_NAME \
  --delete
cd ../

echo "Invalidate cache"
aws cloudfront create-invalidation \
  --paths "/*" \
  --distribution-id $CLOUDFRONT_ID

echo "=> Deployed to $CLOUDFRONT_DOMAIN"
