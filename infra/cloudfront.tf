resource "aws_cloudfront_origin_access_identity" "main" {
  comment = "Created for ${var.app_name}"
}

resource "aws_cloudfront_distribution" "main" {
  enabled          = true
  http_version     = "http2"
  price_class      = "PriceClass_100"
  retain_on_delete = true
  is_ipv6_enabled  = true
  origin {
    origin_id   = "ms-s3-origin"
    domain_name = "${aws_s3_bucket.ms.bucket_domain_name}"
    s3_origin_config {
      origin_access_identity = "${aws_cloudfront_origin_access_identity.main.cloudfront_access_identity_path}"
    }
  }
  origin {
    origin_id   = "treasure-s3-origin"
    domain_name = "${aws_s3_bucket.treasure.bucket_domain_name}"
    s3_origin_config {
      origin_access_identity = "${aws_cloudfront_origin_access_identity.main.cloudfront_access_identity_path}"
    }
  }
  default_cache_behavior {
    target_origin_id       = "ms-s3-origin"
    allowed_methods        = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods         = ["GET", "HEAD"]
    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
    compress               = true
    lambda_function_association {
      event_type = "origin-request"
      lambda_arn = "${aws_lambda_function.rewrite.qualified_arn}"
    }
    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
  }

  ordered_cache_behavior {
    path_pattern           = "/treasure"
    allowed_methods        = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods         = ["GET", "HEAD"]
    target_origin_id       = "treasure-s3-origin"
    viewer_protocol_policy = "redirect-to-https"

    lambda_function_association {
      event_type = "origin-request"
      lambda_arn = "${aws_lambda_function.rewrite.qualified_arn}"
    }

    forwarded_values {
      query_string = false
      headers      = ["Origin"]

      cookies {
        forward = "none"
      }
    }

    min_ttl     = 0
    default_ttl = 86400
    max_ttl     = 31536000
    compress    = true
  }

  ordered_cache_behavior {
    path_pattern           = "/treasure/*"
    allowed_methods        = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods         = ["GET", "HEAD"]
    target_origin_id       = "treasure-s3-origin"
    viewer_protocol_policy = "redirect-to-https"

    lambda_function_association {
      event_type = "origin-request"
      lambda_arn = "${aws_lambda_function.rewrite.qualified_arn}"
    }

    forwarded_values {
      query_string = false
      headers      = ["Origin"]

      cookies {
        forward = "none"
      }
    }

    min_ttl     = 0
    default_ttl = 86400
    max_ttl     = 31536000
    compress    = true
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
}
