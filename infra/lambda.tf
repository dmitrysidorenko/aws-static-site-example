data "archive_file" "rewrite" {
  type        = "zip"
  output_path = "${path.module}/.zip/rewrite.zip"
  source {
    filename = "index.js"
    content  = "${file("${path.module}/../cloud/rewrite.js")}"
  }
  source {
    filename = "config_default.json"
    content  = "${file("${path.module}/../cloud/config_default.json")}"
  }
}

resource "aws_lambda_function" "rewrite" {
  function_name    = "${var.lambda_function_name}"
  filename         = "${data.archive_file.rewrite.output_path}"
  source_code_hash = "${data.archive_file.rewrite.output_base64sha256}"
  role             = "${aws_iam_role.main.arn}"
  runtime          = "nodejs10.x"
  handler          = "index.handler"
  memory_size      = 128
  timeout          = 3
  publish          = true
  depends_on       = ["aws_iam_role_policy_attachment.lambda_logs", "aws_iam_role_policy_attachment.s3_full_access", "aws_cloudwatch_log_group.example"]
}

# This is to optionally manage the CloudWatch Log Group for the Lambda Function.
# If skipping this resource configuration, also add "logs:CreateLogGroup" to the IAM policy below.
resource "aws_cloudwatch_log_group" "example" {
  name              = "/aws/lambda/${var.lambda_function_name}"
  retention_in_days = 14
}
