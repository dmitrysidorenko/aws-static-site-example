data "aws_iam_policy_document" "ms_s3" {
  statement {
    actions = [
      "s3:ListBucket",
      "s3:GetObject"
    ]
    resources = [
      "${aws_s3_bucket.ms.arn}",
      "${aws_s3_bucket.ms.arn}/*",
    ]
    principals {
      type        = "AWS"
      identifiers = ["${aws_cloudfront_origin_access_identity.main.iam_arn}"]
    }
  }
}
data "aws_iam_policy_document" "treasure_s3" {
  statement {
    actions = [
      "s3:ListBucket",
      "s3:GetObject"
    ]
    resources = [
      "${aws_s3_bucket.treasure.arn}",
      "${aws_s3_bucket.treasure.arn}/*",
    ]
    principals {
      type        = "AWS"
      identifiers = ["${aws_cloudfront_origin_access_identity.main.iam_arn}"]
    }
  }
}
data "aws_iam_policy_document" "config_s3" {
  statement {
    actions = [
      "s3:ListBucket",
      "s3:GetObject",
      "s3:PutObject",
    ]
    resources = [
      "${aws_s3_bucket.config.arn}",
      "${aws_s3_bucket.config.arn}/*"
    ]
    principals {
      type        = "AWS"
      identifiers = ["${aws_cloudfront_origin_access_identity.main.iam_arn}"]
    }
  }
}

resource "aws_s3_bucket" "ms" {
  bucket_prefix       = "${var.ms_app_name}"
  acl                 = "private"
  force_destroy       = true
  acceleration_status = "Enabled"
}
resource "aws_s3_bucket" "treasure" {
  bucket_prefix       = "${var.treasure_app_name}"
  acl                 = "private"
  force_destroy       = true
  acceleration_status = "Enabled"
}
resource "aws_s3_bucket" "config" {
  bucket_prefix       = "${var.app_name}"
  acl                 = "private"
  force_destroy       = true
  acceleration_status = "Enabled"
}
resource "aws_s3_bucket_policy" "ms" {
  bucket = "${aws_s3_bucket.ms.id}"
  policy = "${data.aws_iam_policy_document.ms_s3.json}"
}
resource "aws_s3_bucket_policy" "treasure" {
  bucket = "${aws_s3_bucket.treasure.id}"
  policy = "${data.aws_iam_policy_document.treasure_s3.json}"
}
resource "aws_s3_bucket_policy" "config" {
  bucket = "${aws_s3_bucket.config.id}"
  policy = "${data.aws_iam_policy_document.config_s3.json}"
}

resource "aws_s3_bucket_object" "routings" {
  bucket = "${aws_s3_bucket.config.id}"
  key    = "config.json"
  source = "${path.module}/../cloud/config.json"

  # The filemd5() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the md5() function and the file() function:
  # etag = "${md5(file("path/to/file"))}"
  etag = "${filemd5("${path.module}/../cloud/config.json")}"
}
