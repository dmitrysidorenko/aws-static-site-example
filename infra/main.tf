variable "app_name" {
  default = "super-static-on-edge"
}
variable "ms_app_name" {
  default = "ms-super-static-on-edge"
}
variable "treasure_app_name" {
  default = "treasure-super-static-on-edge"
}
variable "lambda_function_name" {
  default = "super-static-on-edge-rewrite"
}


variable "profile" {
  default = "personal"
}

provider "aws" {
  region  = "us-east-1"
  profile = "${var.profile}"
}

output "ms_bucket_name" {
  value = "${aws_s3_bucket.ms.id}"
}

output "treasure_bucket_name" {
  value = "${aws_s3_bucket.treasure.id}"
}
output "config_bucket_name" {
  value = "${aws_s3_bucket.config.id}"
}

output "cloudfront_domain" {
  value = "${aws_cloudfront_distribution.main.domain_name}"
}

output "cloudfront_id" {
  value = "${aws_cloudfront_distribution.main.id}"
}
