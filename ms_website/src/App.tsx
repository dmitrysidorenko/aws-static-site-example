import React from "react";
import { BrowserRouter, Switch, Route, NavLink } from "react-router-dom";
import { FlagsProvider, PersistedDashboard, useFlag } from "./flags";
import logo from "./logo.svg";
import "./App.css";

function Menu() {
  const home = useFlag("homeLinkText");
  const showSecond = useFlag("show_second_site_in_menu");
  return (
    <div style={{ padding: 10, display: "flex", flexDirection: "column" }}>
      <NavLink to="/" exact>
        {home}
      </NavLink>
      <NavLink to="/profile">Profile</NavLink>
      {showSecond && (
        <a href="/treasure" rel="noopener noreferrer">
          Second
        </a>
      )}
    </div>
  );
}

function Logo() {
  const bigLogo = useFlag("bigLogo");
  return (
    <img
      src={logo}
      className="App-logo"
      alt="logo"
      style={{
        ...(bigLogo && {
          height: "80%"
        })
      }}
    />
  );
}

const useTheme = () =>
  useFlag("lightTheme") ? "Theme_Light" : "Theme_Default";

function Home() {
  const theme = useTheme();
  return (
    <div className={`App ${theme}`}>
      <header className="App-header">
        <h1>First > Home</h1>
        <Menu />
        <Logo />
      </header>
    </div>
  );
}

function Profile() {
  const theme = useTheme();
  return (
    <div className={`App ${theme}`}>
      <header className="App-header">
        <h1>First > Profile</h1>
        <Menu />
        <Logo />
      </header>
    </div>
  );
}

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Home}></Route>
      <Route path="/profile" component={Profile}></Route>
      <Route component={Home}></Route>
    </Switch>
  </BrowserRouter>
);

const App: React.FC = () => {
  return (
    <FlagsProvider>
      <>
        <Routes />
        <PersistedDashboard />
      </>
    </FlagsProvider>
  );
};

export default App;
