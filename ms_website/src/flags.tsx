import { zeroConfigCreateFlags } from "feature-flags";

const defaultFlags = {
  show_second_site_in_menu: true,
  bigLogo: false,
  lightTheme: false,
  homeLinkText: "Home"
};

const {
  Dashboard,
  Flag,
  FlagsProvider,
  PersistedDashboard,
  useFeaturesApi,
  useFlag,
  useFlags,
  usePersistFlags
} = zeroConfigCreateFlags<typeof defaultFlags>()(defaultFlags);

export {
  Dashboard,
  Flag,
  FlagsProvider,
  PersistedDashboard,
  useFeaturesApi,
  useFlag,
  useFlags,
  usePersistFlags
};
