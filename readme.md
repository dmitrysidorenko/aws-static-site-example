# AWS Static

Inspired by [Supercharging a static site with Lambda@Edge](https://medium.com/@jsonmaur/supercharging-a-static-site-with-lambda-edge-da5a1314238b).

Host two SPA website under single domain - under a pathname

### Dependencies

- [Terraform](https://www.terraform.io/downloads.html)
- [AWS CLI](https://aws.amazon.com/cli/)

### Deploy infrastructure

```bash
$ ./deploy-infra.sh -var "profile=PROFILE_NAME"
```

### Deploy websites

```bash
$ ./deploy-web.sh PROFILE_NAME
```
