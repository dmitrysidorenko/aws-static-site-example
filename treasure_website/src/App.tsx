import React from "react";
import { BrowserRouter, Switch, Route, NavLink } from "react-router-dom";
import logo from "./logo.svg";
import "./App.css";

function Menu() {
  return (
    <div style={{ padding: 10, display: "flex", flexDirection: "column" }}>
      <NavLink to="/" exact>Home</NavLink>
      <NavLink to="/profile">Profile</NavLink>
      <a href="/" rel="noopener noreferrer">
        First
      </a>
    </div>
  );
}

function Home() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Second > Home</h1>
        <Menu />
        <img src={logo} className="App-logo" alt="logo" />
      </header>
    </div>
  );
}

function Profile() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Second > Profile</h1>
        <Menu />
        <img src={logo} className="App-logo" alt="logo" />
      </header>
    </div>
  );
}

const Routes = () => (
  <BrowserRouter basename={process.env.PUBLIC_URL}>
    <Switch>
      <Route exact path="/" component={Home}></Route>
      <Route path="/profile" component={Profile}></Route>
      <Route component={Home}></Route>
    </Switch>
  </BrowserRouter>
);

const App: React.FC = () => {
  return <Routes />;
};

export default App;
